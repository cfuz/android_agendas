package com.example.agendas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class SettingsActivity extends AppCompatActivity {

    private Button btn_apply, btn_cancel;
    private EditText et_url;
    private SharedPreferences shared_preferences;
    public static final String USER_PREF = "user_preferences", URL = "url", GROUP = "group", BFF = "bff";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings); // Définit le layout associé à la classe
        // Instanciation des champs d'interaction avec l'utilisateur
        et_url = (EditText) findViewById(R.id.edittext_url);
        btn_apply = (Button) findViewById(R.id.settings_apply_button);
        btn_cancel = (Button) findViewById(R.id.settings_cancel_button);
        shared_preferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        // shared_preferences.edit().clear().commit(); // Pour vider le cache des préférences utilisateur pour tests:
        /* Gestion du contenu des champs textuels avec les préférences utilisateur si celles ci existent */
        String url = shared_preferences.getString(URL, "");
        if (!url.equals("")) // Si une URL est déjà définie on pré-remplit le champ correspondant
            et_url.setText(url);
        // Gestion des boutons de l'activité
        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save()) // On termine l'activité uniquement si l'utilisateur à remplit les deux champs
                    startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                finish();
            }
        });
        if (shared_preferences.getString(URL, "").equals(""))
            btn_cancel.setVisibility(View.GONE);
        else
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                    finish();
                }
            });
    }

    /**
     * Sauvegarde du paramètrage utilisateur dans le fichier user_preferences.xml des SharedPreferences
     */
    private boolean save() {
        String toast_alert = "";
        if (et_url.getText() != null && !et_url.getText().toString().equals("")) { // On enregistre seulement les champs qui ont été modifiés
            try {
                java.net.URL ical_url = new URL(et_url.getText().toString()); // On récupère l'URL pour vérifier si celle-ci est valide, sinon on récupère l'erreur et on affiche un Toast d'erreur
                if (!shared_preferences.getString(URL, "").equals(et_url.getText().toString())) {
                    shared_preferences.edit().putString(URL, et_url.getText().toString()).commit();
                    toast_alert += "Nouvelle url enregistrée avec succès !";
                }
                Toast.makeText(
                        getApplicationContext(),
                        toast_alert.isEmpty() ? "Aucune modification à apporter ..." : toast_alert,
                        Toast.LENGTH_SHORT
                ).show();
                return true;
            } catch (MalformedURLException url_error) { // Si l'URL renseignée n'a pas un format valide, on le signale à l'utilisateur et on retourne false
                Toast.makeText(
                        getApplicationContext(),
                        "L'url vers votre emploi du temps n'est pas une url valide.\nVeuillez réessayer avec une autre url.",
                        Toast.LENGTH_SHORT
                ).show();
                url_error.printStackTrace(); // On affiche le log d'erreur dans la console
                return false;
            }
        } else {
            if (et_url.getText() != null && et_url.getText().toString().equals("") && shared_preferences.getString(URL, "").equals("")) {
                Toast.makeText(
                        getApplicationContext(),
                        "Aucune url n'est renseignée, ni dans le champ ci-dessus, ni dans les préférences.\nVeuillez renseigner le champ nécessaire.",
                        Toast.LENGTH_LONG
                ).show();
                return false;
            } else {
                if (et_url.getText() != null && !et_url.getText().toString().equals("")) {
                    try {
                        java.net.URL ical_url = new URL(et_url.getText().toString()); // On récupère l'URL pour vérifier si celle-ci est valide, sinon on récupère l'erreur et on affiche un Toast d'erreur
                        shared_preferences.edit().putString(URL, et_url.getText().toString()).commit();
                        toast_alert = "Nouvelle url enregistrée avec succès !";
                    } catch (MalformedURLException url_error) { // Si l'URL renseignée n'a pas un format valide, on le signale à l'utilisateur et on retourne false
                        Toast.makeText(
                                getApplicationContext(),
                                "L'url vers votre emploi du temps n'est pas une url valide.\nVeuillez réessayer avec une autre url.",
                                Toast.LENGTH_SHORT
                        ).show();
                        url_error.printStackTrace(); // On affiche le log d'erreur dans la console
                        return false;
                    }
                } else
                    toast_alert = "Aucune modification à apporter ...";
                Toast.makeText(
                        getApplicationContext(),
                        toast_alert,
                        Toast.LENGTH_SHORT
                ).show();
                return true;
            }
        }
    }
}

package com.example.agendas.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class iCalParser {

    private BufferedReader buff;

    public iCalParser(BufferedReader buff) throws IOException {
        this.buff = buff;
    }

    /**
     * Récupère le nom de la promotion
     *
     * @return
     * @throws IOException
     */
    public String NomPromo() throws IOException {
        BufferedReader in = buff;
        String line = null;
        try {
            while ((line = in.readLine()) != null) {
                if (line.contains("Emploi du temps de ")) {
                    String[] l = line.split("<");
                    l = l[1].split(">");
                    return l[0];
                }
            }
            in.close();
            return "";
        } catch (IndexOutOfBoundsException e) {
            return "---";
        }
    }

    public String NomGroupe() throws IOException {
        BufferedReader in = buff;
        String line = null;
        try {
            while ((line = in.readLine()) != null) {
                if (line.contains("Emploi du temps de ")) {
                    String[] l = line.split("<TD>");
                    l = l[1].split("-");
                    l = l[2].split("généré par");
                    return l[0].replace("_", " ");
                }
            }
            in.close();
            return "";
        } catch (IndexOutOfBoundsException e) {
            return "--";
        }
    }

    public boolean SalleLibr(Date entr) throws IOException, ParseException {
        BufferedReader in = buff;
        String line;
        SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
        while ((line = in.readLine()) != null) {
            if (line.startsWith("DTSTART:" + form.format(entr))) {
                Matiere m = new Matiere();
                String[] debut = line.split(":");
                String debutt = debut[1];
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                Date deb = sdf.parse(debutt);
                Date h = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                h = dateFormat.parse("01/04/2019 00:00");
                if (deb.before(h)) {
                    deb.setHours(deb.getHours() + 1);
                } else if (deb.after(h)) {
                    deb.setHours(deb.getHours() + 2);
                }
                m.setDebut(deb);
                line = in.readLine();
                String[] fin = line.split(":");
                String finn = fin[1];
                Date ffin = sdf.parse(finn);
                if (ffin.before(h)) {
                    ffin.setHours(ffin.getHours() + 1);
                } else if (ffin.after(h)) {
                    ffin.setHours(ffin.getHours() + 2);
                }
                m.setFin(ffin);
                if (m.getDebut().before(entr) && m.getFin().after(entr))
                    return false;
            }
        }
        in.close();
        //System.out.println(m.get);
        return true;
    }

    public List<Matiere> NextMat(Date entr) throws IOException, ParseException {
        List<Matiere> empl = new ArrayList<>();
        empl = EmploiAjd(entr);
        for (int i = 0; i < empl.size(); i++) {
            Date temp = (Date) empl.get(i).getDebut().clone();
            temp.setMinutes(temp.getMinutes() + 15);
            if (entr.after(empl.get(i).getDebut()) && entr.before(temp))
                empl.get(i).setFlag(true);
        }
        for (int i = 0; i < empl.size(); i++) {
            if (entr.after(empl.get(i).getDebut()) == true && empl.get(i).isFlag() == false) {
                empl.remove(i);
                i--;
            }
        }
        for (int i = empl.size(); i > 2; i--)
            empl.remove(i - 1);
        return empl;
    }

    /**
     * Retourne la liste des matières à partir d'une date fournie en paramètres et d'un identifiant de groupe
     *
     * @param entr
     * @return Liste des matières pour une date donnée
     * @throws IOException
     * @throws ParseException
     */
    public List<Matiere> EmploiAjd(Date entr) throws IOException, ParseException {
        BufferedReader in = buff;
        String line;
        List<Matiere> emploi = new ArrayList<>();
        SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
        while ((line = in.readLine()) != null) {
            if (line.startsWith("DTSTART:" + form.format(entr))) {
                Matiere m = new Matiere();
                String[] debut = line.split(":");
                String debutt = debut[1];
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                Date deb = sdf.parse(debutt);
                Date h = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                h = dateFormat.parse("01/04/2019 00:00"); // Date de changement d'heure hiver / été
                if (deb.before(h)) {
                    deb.setHours(deb.getHours() + 1);
                } else if (deb.after(h)) {
                    deb.setHours(deb.getHours() + 2);
                }
                m.setDebut(deb);
                line = in.readLine();
                String[] fin = line.split(":");
                String finn = fin[1];
                Date ffin = sdf.parse(finn);
                if (ffin.before(h)) {
                    ffin.setHours(ffin.getHours() + 1);
                } else if (ffin.after(h)) {
                    ffin.setHours(ffin.getHours() + 2);
                }
                m.setFin(ffin);
                line = in.readLine();
                String[] l = line.split(";");
                String ll = null;
                if (l.length > 1) {
                    ll = l[1];
                }
                l = ll.split(":");
                ll = "";
                for (int i = 1; i < l.length; i++) {
                    ll = ll + l[i];
                }
                l = ll.split("-");
                ll = l[0];
                if (ll.contains("UEO")) {
                    m.setNom(ll);
                    m.setProf("");
                    if (l.length > 1) {
                        ll = l[1];
                    }
                    m.setGroupe(ll);
                    m.setType("");
                    m.setSalle("");
                    emploi.add(m);
                } else if (!ll.contains("Annulation")) {
                    m.setNom(ll);
                    if (l.length > 1) {
                        ll = l[1];
                    }
                    m.setProf(ll);
                    System.out.println(ll);
                    System.out.println(l[1]);
                    System.out.println(m.toString());
                    if (l.length > 2) {
                        ll = l[2];
                    }
                    m.setGroupe(ll);
                    ll = l[l.length - 1];
                    m.setType(ll);
                    line = in.readLine();
                    l = line.split(";");
                    if (l.length > 1) {
                        ll = l[1];
                    }
                    l = ll.split(":");
                    if (l.length > 1) {
                        ll = l[1];
                    }
                    m.setSalle(ll);
                    emploi.add(m);
                }
            }
        }
        in.close();
        Collections.sort(emploi, new Comparator<Matiere>() {
            @Override
            public int compare(Matiere o1, Matiere o2) {
                return o1.getDebut().compareTo(o2.getDebut());
            }
        });
        return emploi;
    }


    /**
     * Retourne la liste des prochaines évaluations en fonction d'un numéro de groupe fournit en paramètres
     *
     * @return Une liste de matière de type "Evaluation"
     * @throws IOException
     * @throws ParseException
     */
    public List<Matiere> ProchEval() throws IOException, ParseException {
        //System.out.println("OUI");
        BufferedReader in = buff;
        String line;
        List<Matiere> emploi = new ArrayList<>();
        SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        while ((line = in.readLine()) != null) {
            if (line.startsWith("DTSTART:")) {
                String[] li = line.split(":");
                String lig = li[1];
                li = lig.split("T");
                lig = li[0];
                Date datee = form.parse(lig);
                if (date.before(datee)) {
                    Matiere m = new Matiere();
                    String[] debut = line.split(":");
                    String debutt = debut[1];
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                    Date deb = sdf.parse(debutt);
                    deb.setHours(deb.getHours() + 2);
                    m.setDebut(deb);
                    line = in.readLine();
                    String[] fin = line.split(":");
                    String finn = fin[1];
                    Date ffin = sdf.parse(finn);
                    ffin.setHours(ffin.getHours() + 2);
                    m.setFin(ffin);
                    line = in.readLine();
                    String[] l = line.split(";");
                    String ll = l[1];
                    if (!ll.contains("Annulation") && ll.contains("Evaluation")) {
                        l = ll.split(":");
                        if (l.length > 1) {
                            ll = l[1];
                        }
                        l = ll.split(" - ");
                        ll = l[0];
                        m.setNom(ll);
                        if (l.length > 1) {
                            ll = l[1];
                        }
                        m.setProf(ll);
                        if (l.length > 2) {
                            ll = l[2];
                        }
                        m.setGroupe(ll);
                        if (l.length > 3) {
                            ll = l[3];
                        }
                        m.setType(ll);
                        line = in.readLine();
                        l = line.split(";");
                        if (l.length > 1) {
                            ll = l[1];
                        }
                        l = ll.split(":");
                        if (l.length > 1) {
                            ll = l[1];
                        }
                        m.setSalle(ll);
                        emploi.add(m);
                    }
                }
            }
        }
        in.close();
        Collections.sort(emploi, new Comparator<Matiere>() {
            @Override
            public int compare(Matiere o1, Matiere o2) {
                return o1.getDebut().compareTo(o2.getDebut());
            }
        });
        return emploi;
    }
}
package com.example.agendas.model;

import java.util.Date;

public class Matiere {

    private String _nom;
    private Date _debut;
    private Date _fin;
    private String _prof;
    private String _salle;
    private String _type;
    private String _groupe;
    private boolean _flag;

    public boolean isFlag() {
        return _flag;
    }

    public void setFlag(boolean flag) {
        _flag = flag;
    }

    public String getNom() {
        return _nom;
    }

    public void setNom(String nom) {
        _nom = nom;
    }

    public Date getDebut() {
        return _debut;
    }

    public void setDebut(Date debut) {
        _debut = debut;
    }

    public Date getFin() {
        return _fin;
    }

    public void setFin(Date fin) {
        _fin = fin;
    }

    public String getProf() {
        return _prof;
    }

    public void setProf(String prof) {
        _prof = prof;
    }

    public String getSalle() {
        return _salle;
    }

    public void setSalle(String salle) {
        _salle  = salle;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    @Override
    public String toString() {
        return "Matiere {" +
                "nom='" + _nom + '\'' +
                ", debut=" + _debut +
                ", fin=" + _fin +
                ", prof='" + _prof + '\'' +
                ", salle='" + _salle + '\'' +
                ", type='" + _type + '\'' +
                ", groupe='" + _groupe + '\'' +
                ", flag=" + _flag +
                '}';
    }

    public Matiere() {
        _nom = "";
        _debut = null;
        _fin = null;
        _prof="";
        _salle="";
        _type="";
        _groupe="";
        _flag = false;
    }

    public Matiere (
            Date debut,
            Date fin,
            String type,
            String nom,
            String professeur,
            String salle,
            boolean flag,
            String groupe
    ) {
        _nom = nom;
        _debut = debut;
        _fin = fin;
        _prof = professeur;
        _salle = salle;
        _type = type;
        _groupe = groupe;
        _flag = flag;
    }

    public String getGroupe() {
        return _groupe;
    }

    public void setGroupe(String groupe) {
        _groupe = groupe;
    }

}



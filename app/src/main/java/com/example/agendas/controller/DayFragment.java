package com.example.agendas.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.agendas.R;
import com.example.agendas.CardAdapter;
import com.example.agendas.SettingsActivity;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DayFragment extends Fragment {

    private List<Matiere> edt;
    private CalendarView cv_calendar;
    private RecyclerView rv_courses;
    private CardAdapter rva_courses_adapter;
    private SharedPreferences shared_preferences;
    private AtAgenda async_task_agenda;
    private ProgressBar progress_bar;
    private static SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final String TAG = "agenda";

    /**
     * Fonction appelée à la création du Fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.fragment_day, container, false);
        shared_preferences = this.getActivity().getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        edt = new ArrayList<Matiere>();
        init_layout(root_view);
        /* Récupération de la date sélectionnée par défaut au lancement du fragment (date du jour par défaut) */
        Date d = new Date(cv_calendar.getDate());
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        AtAgenda at_agenda = new AtAgenda(edt, d, rva_courses_adapter);
        at_agenda.execute(shared_preferences.getString(SettingsActivity.URL, ""));
        return root_view;
    }

    /**
     * Fonction appelée une fois le Fragment généré
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (async_task_agenda != null)
            async_task_agenda.cancel(true);
    }

    private void init_layout (View root_view) {
        progress_bar = root_view.findViewById(R.id.day_progress_bar);
        init_calendarview(root_view);
        init_recyclerview(root_view);
        rv_courses.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);
    }

    private void toggle_rl_visibility () {
        rv_courses.setVisibility(View.VISIBLE);
        progress_bar.setVisibility(View.GONE);
    }

    /**
     * Initialisation du calendrier "cv_calendar" (instanciation et définition des listeners)
     * <p>
     * NOTA:
     * Récupération de la date du calendrier via
     * --> date_format.format(cv_calendar.getDate())
     *
     * @param root_view
     */
    private void init_calendarview(View root_view) {
        cv_calendar = root_view.findViewById(R.id.cv_calendar); // Récupération de la référence du calendrier du Fragment
        /* Définition d'un listener pour les interactions avec le calendrier */
        CalendarView.OnDateChangeListener date_change_listener = new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                rv_courses.setVisibility(View.GONE);
                progress_bar.setVisibility(View.VISIBLE);
                String date_str = String.format("%02d", dayOfMonth) + "/" + String.format("%02d", month+1) + "/" + String.format("%03d", year) + " 00:00";
                try {
                    async_task_agenda = new AtAgenda(edt, date_format.parse(date_str), rva_courses_adapter);
                    async_task_agenda.execute(shared_preferences.getString(SettingsActivity.URL, ""));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };
        cv_calendar.setOnDateChangeListener(date_change_listener); // Attribution du listener au CalendarView
    }

    /**
     * Initialise la RecyclerView à la création du Fragment  (nécessite au préalable que l'attribut "edt" soit instancié)
     *
     * @param root_view
     */
    private void init_recyclerview(View root_view) {
        rv_courses = root_view.findViewById(R.id.rv_courses); // Prend la référence vers la RecyclerView
        rv_courses.setLayoutManager(new LinearLayoutManager(getActivity())); // Définition du layout manager pour la gestion de la RecyclerView à laquelle elle est associée
        rva_courses_adapter = new CardAdapter(edt,DayFragment.TAG); // Définition de l'Adapter pour affichage dans la liste
        rv_courses.setAdapter(rva_courses_adapter); // Association de l'Adapter à la RecyclerView définie précédemment
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtAgenda extends AsyncTask<String, Void, List<Matiere>> {

        List<Matiere> _agenda;
        Date _date;
        CardAdapter _adapter;

        AtAgenda(List<Matiere> agenda, Date date, CardAdapter adapter) {
            _agenda = agenda;
            _date = date;
            _adapter = adapter;
        }

        @Override
        protected List<Matiere> doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                iCalParser ic = new iCalParser(in);
                return ic.EmploiAjd(_date);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                if (result != null) {
                    _agenda.clear(); // Supprime les anciennes données de la liste
                    _agenda.addAll(result);
                    _adapter.refresh_items(_agenda);
                    _adapter.notifyDataSetChanged();
                    toggle_rl_visibility();
                } else
                    Toast.makeText(
                            getActivity(),
                            "Il y a eu une erreur lors de l'appel au webservice.\nVeuillez vérifier la validité de votre URL.",
                            Toast.LENGTH_SHORT
                    ).show();
            } else
                Toast.makeText(
                        getActivity(),
                        "Web service annulé ...",
                        Toast.LENGTH_SHORT
                ).show();
        }

    }

}

package com.example.agendas.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.agendas.CardAdapter;
import com.example.agendas.R;
import com.example.agendas.SettingsActivity;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;

public class ClassroomsFragment extends Fragment {

    public static final String TAG = "classrooms";
    private List<Matiere> edt;
    LinearLayout linear_layout;
    private RecyclerView rv_classrooms;
    CardAdapter rv_classrooms_adapter;
    private SharedPreferences shared_preferences;
    private AtClassrooms async_task_classrooms;
    ProgressBar progress_bar;
    private static SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.fragment_classrooms, container, false);
        shared_preferences = this.getActivity().getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        edt = new ArrayList<Matiere>();
        init_layout(root_view); // Initialisation de la RecyclerView
        async_task_classrooms = new AtClassrooms(edt, rv_classrooms_adapter);
        async_task_classrooms.execute(shared_preferences.getString(SettingsActivity.URL, ""));
        return root_view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (async_task_classrooms != null)
            async_task_classrooms.cancel(true);
    }

    private void init_layout (View root_view) {
        progress_bar = root_view.findViewById(R.id.classrooms_progress_bar);
        linear_layout = root_view.findViewById(R.id.classrooms_fragment_linearlayout);
        init_recyclerview(root_view); // Initialisation de la RecyclerView
        progress_bar.setVisibility(View.VISIBLE);
        linear_layout.setVisibility(View.GONE);
    }

    private void toggle_visibility() {
        progress_bar.setVisibility(View.GONE);
        linear_layout.setVisibility(View.VISIBLE);
    }

    /**
     * Initialise la RecyclerView à la création du Fragment  (nécessite au préalable que l'attribut "edt" soit instancié)
     *
     * @param root_view
     */
    private void init_recyclerview(View root_view) {
        rv_classrooms = root_view.findViewById(R.id.rv_classrooms); // Prend la référence vers la RecyclerView
        rv_classrooms.setLayoutManager(new LinearLayoutManager(getActivity())); // Définition du layout manager pour la gestion de la RecyclerView à laquelle elle est associée
        rv_classrooms_adapter = new CardAdapter(edt, ClassroomsFragment.TAG); // Définition de l'Adapter pour affichage dans la liste
        rv_classrooms.setAdapter(rv_classrooms_adapter); // Association de l'Adapter à la RecyclerView définie précédemment
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtClassrooms extends AsyncTask<String, Void, List<Matiere>> {
        List<Matiere> _agenda;
        CardAdapter _adapter;

        AtClassrooms(List<Matiere> agenda, CardAdapter adapter) {
            _agenda = agenda;
            _adapter = adapter;
        }

        @Override
        protected List<Matiere> doInBackground(String... params) {
            List<String> classrooms_links = new ArrayList<>(asList(
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT1",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT2",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT3",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT4",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT5",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT6",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT7",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT8",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_STAT9",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S1",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S2",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S3",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S4",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S5",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S6",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S7",
                    "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/salle/CERI_S8"
            ));
            List<String> classroom_name = new ArrayList<>(asList(
                    "STAT1", "STAT2", "STAT3", "STAT4", "STAT5", "STAT6", "STAT7", "STAT8", "STAT9",
                    "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8"
            ));
            List<Matiere> free_classrooms = new ArrayList<>();
            URL url;
            try {
                Date current_date = new Date();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                for (int i = 0; i < classroom_name.size(); i++) {
                    url = new URL(classrooms_links.get(i));
                    BufferedReader inn = new BufferedReader(new InputStreamReader(url.openStream()));
                    iCalParser ical_parser = new iCalParser(inn);
                    if (ical_parser.SalleLibr(current_date))
                        free_classrooms.add(new Matiere(current_date, current_date, "", "", "", classroom_name.get(i), false, ""));
                }
                if (free_classrooms.isEmpty())
                    free_classrooms.add(new Matiere(null, null, "", "", "", "Aucune salle de disponible pour le créneau actuel :(", false, ""));
                return free_classrooms;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                if (result != null) {
                    _agenda.clear(); // Supprime les anciennes données de la liste
                    _agenda.addAll(result);
                    _adapter.refresh_items(_agenda);
                    _adapter.notifyDataSetChanged();
                    toggle_visibility();
                } else
                    Toast.makeText(
                            getActivity(),
                            "Il y a eu une erreur lors de l'appel au webservice pour récupérer les salles.\nVeuillez vérifier la validité de votre URL.",
                            Toast.LENGTH_SHORT
                    ).show();
            } else
                Toast.makeText(
                        getActivity(),
                        "Web service annulé ...",
                        Toast.LENGTH_SHORT
                ).show();
        }

    }

}

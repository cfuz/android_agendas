package com.example.agendas.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.agendas.R;
import com.example.agendas.CardAdapter;
import com.example.agendas.SettingsActivity;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CurrentFragment extends Fragment {
    private List<Matiere> edt;
    private Date current_date;
    LinearLayout linear_layout;
    private RecyclerView rv_courses;
    CardAdapter rva_courses_adapter;
    private SharedPreferences shared_preferences;
    private AtCurrent async_task_current;
    private ProgressBar progress_bar;
    private static SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");
    public static final String TAG = "current";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.fragment_current, container, false);
        shared_preferences = this.getActivity().getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        edt = new ArrayList<Matiere>();
        current_date = Calendar.getInstance().getTime();
        current_date.setHours(0);
        current_date.setMinutes(0);
        current_date.setSeconds(0);
        init_layout(root_view);
        async_task_current = new AtCurrent(edt, current_date, rva_courses_adapter);
        async_task_current.execute(shared_preferences.getString(SettingsActivity.URL, ""));
        return root_view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (async_task_current != null)
            async_task_current.cancel(true);
    }

    private void init_layout (View root_view) {
        progress_bar = root_view.findViewById(R.id.current_progress_bar);
        linear_layout = root_view.findViewById(R.id.current_fragment_linearlayout);
        progress_bar.setVisibility(View.VISIBLE);
        linear_layout.setVisibility(View.GONE);
        init_recyclerview(root_view); // Initialisation de la RecyclerView
    }

    /**
     * Initialise la RecyclerView à la création du Fragment  (nécessite au préalable que l'attribut "edt" soit instancié)
     *
     * @param root_view
     */
    private void init_recyclerview(View root_view) {
        rv_courses = root_view.findViewById(R.id.rv_courses); // Prend la référence vers la RecyclerView
        rv_courses.setLayoutManager(new LinearLayoutManager(getActivity())); // Définition du layout manager pour la gestion de la RecyclerView à laquelle elle est associée
        rva_courses_adapter = new CardAdapter(edt, CurrentFragment.TAG); // Définition de l'Adapter pour affichage dans la liste
        rv_courses.setAdapter(rva_courses_adapter); // Association de l'Adapter à la RecyclerView définie précédemment
    }

    private void toggle_visibility () {
        progress_bar.setVisibility(View.GONE);
        linear_layout.setVisibility(View.VISIBLE);
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtCurrent extends AsyncTask<String, Void, List<Matiere>> {
        List<Matiere> _agenda;
        Date _date;
        CardAdapter _adapter;

        AtCurrent(List<Matiere> agenda, Date date, CardAdapter adapter) {
            _agenda = agenda;
            _date = date;
            _adapter = adapter;
        }

        @Override
        protected List<Matiere> doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                iCalParser ic = new iCalParser(in);
                return ic.EmploiAjd(_date);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                if (result != null) {
                    _agenda.clear(); // Supprime les anciennes données de la liste
                    _agenda.addAll(result);
                    _adapter.refresh_items(_agenda);
                    _adapter.notifyDataSetChanged();
                    toggle_visibility();
                } else
                    Toast.makeText(
                            getActivity(),
                            "Il y a eu une erreur lors de l'appel au webservice.\nVeuillez vérifier la validité de votre URL.",
                            Toast.LENGTH_SHORT
                    ).show();
            } else
                Toast.makeText(
                        getActivity(),
                        "Web service annulé ...",
                        Toast.LENGTH_SHORT
                ).show();
        }

    }

}

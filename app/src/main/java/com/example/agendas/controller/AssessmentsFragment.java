package com.example.agendas.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.agendas.CardAdapter;
import com.example.agendas.R;
import com.example.agendas.SettingsActivity;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class AssessmentsFragment extends Fragment {

    public static final String TAG = "assessments";
    private List<Matiere> edt;
    LinearLayout linear_layout;
    private RecyclerView rv_assessments;
    CardAdapter rv_assessments_adapter;
    private SharedPreferences shared_preferences;
    private AtAssessments async_task_assessments;
    private ProgressBar progress_bar;
    private static SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.fragment_assessments, container, false);
        shared_preferences = this.getActivity().getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        edt = new ArrayList<Matiere>();
        init_layout(root_view);
        async_task_assessments = new AtAssessments(edt, rv_assessments_adapter);
        async_task_assessments.execute(shared_preferences.getString(SettingsActivity.URL, ""));
        return root_view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (async_task_assessments != null)
            async_task_assessments.cancel(true);
    }

    private void init_layout (View root_view) {
        progress_bar = root_view.findViewById(R.id.assessments_progress_bar);
        linear_layout = root_view.findViewById(R.id.assessments_fragment_linearlayout);
        init_recyclerview(root_view); // Initialisation de la RecyclerView
        progress_bar.setVisibility(View.VISIBLE);
        linear_layout.setVisibility(View.GONE);
    }

    private void toggle_visibility () {
        progress_bar.setVisibility(View.GONE);
        linear_layout.setVisibility(View.VISIBLE);
    }

    /**
     * Initialise la RecyclerView à la création du Fragment  (nécessite au préalable que l'attribut "edt" soit instancié)
     *
     * @param root_view
     */
    private void init_recyclerview(View root_view) {
        rv_assessments = root_view.findViewById(R.id.rv_assessments); // Prend la référence vers la RecyclerView
        rv_assessments.setLayoutManager(new LinearLayoutManager(getActivity())); // Définition du layout manager pour la gestion de la RecyclerView à laquelle elle est associée
        rv_assessments_adapter = new CardAdapter(edt, AssessmentsFragment.TAG); // Définition de l'Adapter pour affichage dans la liste
        rv_assessments.setAdapter(rv_assessments_adapter); // Association de l'Adapter à la RecyclerView définie précédemment
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtAssessments extends AsyncTask<String, Void, List<Matiere>> {
        List<Matiere> _agenda;
        CardAdapter _adapter;

        AtAssessments(List<Matiere> agenda, CardAdapter adapter) {
            _agenda = agenda;
            _adapter = adapter;
        }

        @Override
        protected List<Matiere> doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                iCalParser ic = new iCalParser(in);
                return ic.ProchEval();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                if (result != null) {
                    _agenda.clear(); // Supprime les anciennes données de la liste
                    _agenda.addAll(result);
                    _adapter.refresh_items(_agenda);
                    _adapter.notifyDataSetChanged();
                    toggle_visibility();
                } else
                    Toast.makeText(
                            getActivity(),
                            "Il y a eu une erreur lors de l'appel au webservice.\nVeuillez vérifier la validité de votre URL.",
                            Toast.LENGTH_SHORT
                    ).show();
            } else
                Toast.makeText(
                        getActivity(),
                        "Web service annulé ...",
                        Toast.LENGTH_SHORT
                ).show();
        }

    }

}
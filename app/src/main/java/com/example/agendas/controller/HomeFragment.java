package com.example.agendas.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agendas.R;
import com.example.agendas.SettingsActivity;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HomeFragment extends Fragment {

    public static final String TAG = "home";
    View root_view;
    private List<Matiere> edt;
    private Date current_date;
    LinearLayout linear_layout;
    RelativeLayout rl_upcoming_event, rl_next_event;
    TextView tv_upcoming_event_start, tv_upcoming_event_end, tv_upcoming_event_course, tv_upcoming_event_classroom, tv_upcoming_event_lecturer, tv_late_status,
            tv_next_event_start, tv_next_event_end, tv_next_event_course, tv_next_event_classroom, tv_next_event_lecturer;
    ImageView img_upcoming_event_type, img_next_event_type;
    private SharedPreferences shared_preferences;
    private AtHome async_task_home;
    private ProgressBar progress_bar;
    private static SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_home, container, false);
        shared_preferences = this.getActivity().getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        edt = new ArrayList<Matiere>();
        current_date = Calendar.getInstance().getTime();
        async_task_home = new AtHome(edt, current_date);
        async_task_home.execute(shared_preferences.getString(SettingsActivity.URL, ""));
        return root_view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init_fields();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (async_task_home != null)
            async_task_home.cancel(true);
    }

    private void init_fields() {
        rl_upcoming_event = root_view.findViewById(R.id.rl_upcoming_event);
        rl_next_event = root_view.findViewById(R.id.rl_next_event);
        tv_upcoming_event_start = root_view.findViewById(R.id.upcoming_event_start);
        tv_upcoming_event_end = root_view.findViewById(R.id.upcoming_event_end);
        tv_upcoming_event_course = root_view.findViewById(R.id.upcoming_event_course);
        tv_upcoming_event_classroom = root_view.findViewById(R.id.upcoming_event_classroom);
        tv_upcoming_event_lecturer = root_view.findViewById(R.id.upcoming_event_lecturer);
        tv_late_status = root_view.findViewById(R.id.late_status);
        tv_next_event_start = root_view.findViewById(R.id.next_event_start);
        tv_next_event_end = root_view.findViewById(R.id.next_event_end);
        tv_next_event_course = root_view.findViewById(R.id.next_event_course);
        tv_next_event_classroom = root_view.findViewById(R.id.next_event_classroom);
        tv_next_event_lecturer = root_view.findViewById(R.id.next_event_lecturer);
        img_upcoming_event_type = root_view.findViewById(R.id.upcoming_event_type);
        img_next_event_type = root_view.findViewById(R.id.next_event_type);
        progress_bar = root_view.findViewById(R.id.home_progress_bar);
        linear_layout = root_view.findViewById(R.id.home_fragment_linearlayout);
        /* Mise en forme par défaut des cartes de la page d'accueil */
        rl_next_event.setVisibility(View.GONE);
        tv_late_status.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);
        linear_layout.setVisibility(View.GONE);
        img_upcoming_event_type.setVisibility(View.GONE); // Ce champ n'est affiché qu'en cas de flag de retard levé dans la liste
        tv_upcoming_event_start.setText("");
        tv_upcoming_event_end.setText("");
        tv_upcoming_event_course.setText("La journée est terminée :)\nA demain !");
        tv_upcoming_event_classroom.setText("");
        tv_upcoming_event_lecturer.setText("");
    }

    private void set_card_upcoming_event_content(Matiere m) {
        int img_resource_layout;
        DateFormat df = new SimpleDateFormat("HH:mm");
        switch (m.getType()) {
            case " CM":
                img_resource_layout = R.drawable.ic_lecture_course_light;
                break;
            case " TD":
                img_resource_layout = R.drawable.ic_tutorial_classes_light;
                break;
            case " TP":
                img_resource_layout = R.drawable.ic_practicals_light;
                break;
            case " Evaluation":
                img_resource_layout = R.drawable.ic_assessments_light;
                break;
            default: // Par défaut on ne met rien dans l'ImageView
                if (m.getNom().contains("UEO"))
                    img_resource_layout = R.drawable.ic_sport_light;
                else
                    img_resource_layout = android.R.color.transparent;
                break;
        }
        img_upcoming_event_type.setImageResource(img_resource_layout);
        img_upcoming_event_type.setVisibility(View.VISIBLE); // Ce champ n'est affiché qu'en cas de flag de retard levé dans la liste
        tv_upcoming_event_start.setText(df.format(m.getDebut()));
        tv_upcoming_event_end.setText(df.format(m.getFin()));
        tv_upcoming_event_course.setText(m.getNom());
        tv_upcoming_event_classroom.setText(m.getSalle());
        tv_upcoming_event_lecturer.setText(m.getProf());
        if (m.isFlag()) // Si le flag est levé on affiche le label de retard
            tv_late_status.setVisibility(View.VISIBLE);
    }

    private void set_next_card_event_content(Matiere m) {
        int img_resource_layout;
        DateFormat df = new SimpleDateFormat("HH:mm");
        switch (m.getType()) {
            case " CM":
                img_resource_layout = R.drawable.ic_lecture_course;
                break;
            case " TD":
                img_resource_layout = R.drawable.ic_tutorial_classes;
                break;
            case " TP":
                img_resource_layout = R.drawable.ic_practicals;
                break;
            case " Evaluation":
                img_resource_layout = R.drawable.ic_assessments;
                break;
            default: // Par défaut on ne met rien dans l'ImageView
                if (m.getNom().contains("UEO"))
                    img_resource_layout = R.drawable.ic_sport;
                else
                    img_resource_layout = android.R.color.transparent;
                break;
        }
        img_next_event_type.setImageResource(img_resource_layout);
        img_upcoming_event_type.setVisibility(View.VISIBLE); // Ce champ n'est affiché qu'en cas de flag de retard levé dans la liste
        tv_next_event_start.setText(df.format(m.getDebut()));
        tv_next_event_end.setText(df.format(m.getFin()));
        tv_next_event_course.setText(m.getNom());
        tv_next_event_classroom.setText(m.getSalle());
        tv_next_event_lecturer.setText(m.getProf());
        rl_next_event.setVisibility(View.VISIBLE);
    }


    /**
     * Initialise les cartes à partir de paire de matière retournée par l'AsyncTask
     */
    public void update_cards() {
        if (!edt.isEmpty()) {
            set_card_upcoming_event_content(edt.get(0));
            if (edt.size() > 1)
                set_next_card_event_content(edt.get(1));
        }
        progress_bar.setVisibility(View.GONE);
        linear_layout.setVisibility(View.VISIBLE);
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtHome extends AsyncTask<String, Void, List<Matiere>> {

        List<Matiere> _agenda;
        Date _date;

        AtHome(List<Matiere> agenda, Date date) {
            _agenda = agenda;
            _date = date;
        }

        @Override
        protected List<Matiere> doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                iCalParser ic = new iCalParser(in);
                return ic.NextMat(_date);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                if (result != null) {
                    _agenda.clear(); // Supprime les anciennes données de la liste
                    _agenda.addAll(result);
                    update_cards();
                } else
                    Toast.makeText(
                            getActivity(),
                            "Il y a eu une erreur lors de l'appel au webservice.\nVeuillez vérifier la validité de votre URL.",
                            Toast.LENGTH_SHORT
                    ).show();
            } else
                Toast.makeText(
                        getActivity(),
                        "Web service annulé ...",
                        Toast.LENGTH_SHORT
                ).show();
        }

    }

}

package com.example.agendas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agendas.controller.DayFragment;
import com.example.agendas.controller.AssessmentsFragment;
import com.example.agendas.controller.ClassroomsFragment;
import com.example.agendas.controller.CurrentFragment;
import com.example.agendas.controller.HomeFragment;
import com.example.agendas.model.Matiere;
import com.example.agendas.model.iCalParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    DrawerLayout content_wrapper;
    Toolbar app_header;
    TextView tv_promotion, tv_group;
    SharedPreferences shared_preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Vérification de première connexion / bon paramètrage utilisateur
        shared_preferences = getSharedPreferences(SettingsActivity.USER_PREF, Context.MODE_PRIVATE);
        /**
         * Pour vider le cache des préférences utilisateur pour tests:
         * shared_preferences.edit().clear().commit();
         */
        if (shared_preferences.getString(SettingsActivity.URL, "").equals("")) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class)); // Si les champs SettingsActivity.URL et SettingsActivity.GROUP ne sont pas renseignés / générés on envoit l'utilisateur vers l'activité de paramètrage de son emploi du temps)
            finish();
        }
        /* Définition du fragment à la création de l'activité (par défaut il s'agit du fragment "fragment_home") */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // Tout comme un Intent, on génère un objet permettant d'échanger des données entre les fragments et définissant également le layout du conteneur de fragment "fragment_container.
        transaction.replace(R.id.fragment_container, new HomeFragment()); // On change le layout du conteneur principal de l'application
        transaction.commit(); // On valide les changements apportés au layout
        /* Instanciation et génération du header de l'application */
        app_header = (Toolbar) findViewById(R.id.app_header); // Récupère la référence vers l'en-tête personnalisée de l'application
        app_header.setTitle(R.string.home); // Définition du titre de l'en-tête avec le nom du fragment actuel
        setSupportActionBar(app_header); // Redéfinition de l'en-tête par défaut de l'application
        app_header.setNavigationOnClickListener(new View.OnClickListener() { // Implémentation de l'événement "clic sur l'icône de menu"
            @Override
            public void onClick(View v) { // Ajout d'un onClickListener pour gestion de l'affichae du panneau latéral lorsque l'on appuie sur le bouton type hamburger
                content_wrapper.openDrawer(Gravity.LEFT); // Ouverture du panneau latéral depuis la droite
            }
        });
        /* Génération des entités du panneau latéral */
        init_side_pane();
        /* Création d'une nouvelle tâche de recueil d'informations du calendrier de l'UAPV */
        AtMainActivity t = new AtMainActivity(tv_promotion, tv_group);
        t.execute(shared_preferences.getString(SettingsActivity.URL, ""));
    }

    /**
     * Permet d'hydrater la table des options disponibles dans l'en-tête d'application
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_header_default_menu, menu); // Ajoute le bouton permettant de se rendre sur les paramètres de l'application dans l'en-tête
        return true;
    }

    /**
     * Gestion du click sur le rouage de l'en-tête d'application (changement d'activité MainActivity --> SettingsActivity)
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int selection_id = item.getItemId();
        if (selection_id == R.id.settings_button) { // Si l'option du header sélectionnée est celle des paramètres d'application
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent); // On charge et lance la nouvelle activité
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Génération du panneau latéral ainsi que des actions le concernant (bascule entre les différentes fonctions métier)
     */
    private void init_side_pane() {
        content_wrapper = (DrawerLayout) findViewById(R.id.content_wrapper); // Initialisation de l'enveloppe de l'application
        final NavigationView side_pane = (NavigationView) findViewById(R.id.side_pane); // Initialisation du panneau latéral
        side_pane.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment view_to_load = null; // Génération du nouveau fragment à charger
                String activity_label = "";
                int selection_id = menuItem.getItemId(); // Récupère l'id associé à l'option sélectionnée dans le panneau latéral
                switch (selection_id) { // Chargement de la vue / fragment adéquat en fonction de l'option selectionnée
                    case R.id.nav_home:
                        view_to_load = new HomeFragment();
                        activity_label = getResources().getString(R.string.home);
                        break;
                    case R.id.nav_day:
                        view_to_load = new CurrentFragment();
                        activity_label = getResources().getString(R.string.current);
                        break;
                    case R.id.nav_agenda:
                        view_to_load = new DayFragment();
                        activity_label = getResources().getString(R.string.day);
                        break;
                    case R.id.nav_assessments:
                        view_to_load = new AssessmentsFragment();
                        activity_label = getResources().getString(R.string.assessments);
                        break;
                    case R.id.nav_classrooms:
                        view_to_load = new ClassroomsFragment();
                        activity_label = getResources().getString(R.string.classrooms);
                        break;
                }
                if (view_to_load != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, view_to_load); // On change le layout du conteneur principal de l'application
                    transaction.commit(); // On valide les changements apportés au layout
                    app_header.setTitle(activity_label); // On met à jour le titre de l'en-tête en fonction du nom du fragment à charger
                    content_wrapper.closeDrawers();
                    return true;
                }
                return false;
            }
        });
        /* MàJ du label d'en-tête du panneau latéral*/
        View nav_header = side_pane.inflateHeaderView(R.layout.nav_header);
        tv_promotion = nav_header.findViewById(R.id.promotion);
        tv_group = nav_header.findViewById(R.id.group_id);
        tv_group.setVisibility(View.GONE);
        tv_group.setVisibility(View.GONE);
    }

    private void toggle_visibility() {
        tv_group.setVisibility(View.VISIBLE);
        tv_group.setVisibility(View.VISIBLE);
    }

    /**
     * Classe asynchrone permettant de générer la liste des matières extradées de l'iCal
     */
    private class AtMainActivity extends AsyncTask<String, Void, List<Matiere>> {

        TextView promotion_label, group_label;

        public AtMainActivity(TextView promotion, TextView group) {
            promotion_label = promotion;
            group_label = group;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        protected List<Matiere> doInBackground(String... strings) {
            try {
                if (!strings[0].equals("")) {
                    URL url = new URL(strings[0]);
                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    iCalParser ic = new iCalParser(in);
                    promotion_label.setText(ic.NomPromo());
                    in = new BufferedReader(new InputStreamReader(url.openStream()));
                    ic = new iCalParser(in);
                    group_label.setText(ic.NomGroupe());
                    in = new BufferedReader(new InputStreamReader(url.openStream()));
                    ic = new iCalParser(in);
                    return ic.ProchEval();
                } else
                    return null;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Matiere> result) {
            if (!isCancelled()) {
                toggle_visibility();
                if (result != null) {
                    if (result.size() != 0) {
                        Date d = new Date();
                        d.setDate(d.getDate() + 7);
                        if (result.get(0).getDebut().before(d)) {
                            SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                            Toast.makeText(
                                    MainActivity.this,
                                    "Vous avez un examen dans moins d'une semaine:\n" +
                                            result.get(0).getNom() + "\nLe " +
                                            date_format.format(result.get(0).getDebut()),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
                }
            } else
                Toast.makeText(
                        MainActivity.this,
                        "Aucun partiel n'est à prévoir à moins d'une semaine :)",
                        Toast.LENGTH_LONG
                ).show();

        }

    }

}
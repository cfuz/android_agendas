package com.example.agendas;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.agendas.controller.AssessmentsFragment;
import com.example.agendas.controller.ClassroomsFragment;
import com.example.agendas.controller.CurrentFragment;
import com.example.agendas.controller.DayFragment;
import com.example.agendas.model.Matiere;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private int resource_layout; // Pour l'affichage des cartes en fonction du fragment qui appel cette classe
    private ArrayList<ArrayList<String>> items; // Attribut contenant toutes les informations à afficher pour chaque ligne de l'adapter

    public CardAdapter(List<Matiere> matieres, String mode) {
        switch (mode) {
            case CurrentFragment.TAG:
                resource_layout = R.layout.row_current;
                break;
            case DayFragment.TAG:
                resource_layout = R.layout.row_day;
                break;
            case AssessmentsFragment.TAG:
                resource_layout = R.layout.row_assessments;
                break;
            case ClassroomsFragment.TAG:
                resource_layout = R.layout.row_classrooms;
                break;
        }
        items = new ArrayList<ArrayList<String>>();
        refresh_items(matieres);
    }

    /**
     * Création d'une ViewHolder à l'intérieur de notre Adapter pour assigner le template et instancié les champs nécessaires
     * pour chaque ligne à afficher
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rl_container;
        private TextView tv_event_start, tv_event_end, tv_event_course, tv_event_classroom, tv_event_lecturer, tv_assessment_date;
        private ImageView img_event_type;

        public ViewHolder(View itemView) {
            super(itemView);
            rl_container = (RelativeLayout) itemView.findViewById(R.id.course_container); // Conteneur d'une ligne de l'adapter
            /* Instanciation des champs affichés par chaque ligne de l'adapter */
            if (resource_layout == R.layout.row_assessments)
                tv_assessment_date = (TextView) itemView.findViewById(R.id.assessment_date);
            if (resource_layout != R.layout.row_classrooms) {
                tv_event_start = (TextView) itemView.findViewById(R.id.event_start);
                tv_event_end = (TextView) itemView.findViewById(R.id.event_end);
                tv_event_course = (TextView) itemView.findViewById(R.id.event_course);
            }
            tv_event_classroom = (TextView) itemView.findViewById(R.id.event_classroom);
            if (resource_layout == R.layout.row_current || resource_layout == R.layout.row_day) {
                tv_event_lecturer = (TextView) itemView.findViewById(R.id.event_lecturer);
                img_event_type = (ImageView) itemView.findViewById(R.id.event_type); // Type de cours représenté par une image
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view_type) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(resource_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * Cette fonction nous permet d'attribuer les valeurs correspondantes à chacune des lignes de notre liste "items"
     * à chacun des champs répertoriés dans la classe interne ViewHolder définie plus haut
     *
     * @param viewHolder
     * @param item_index
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int item_index) {
        if (!items.isEmpty() && items.get(0).size() > 1) {
            if (resource_layout == R.layout.row_assessments) {
                viewHolder.tv_assessment_date.setText(items.get(item_index).get(6));
                viewHolder.tv_assessment_date.setVisibility(View.VISIBLE);
            }
            if (resource_layout != R.layout.row_classrooms) {
                viewHolder.tv_event_start.setText(items.get(item_index).get(0));
                viewHolder.tv_event_end.setText(items.get(item_index).get(1));
                viewHolder.tv_event_course.setText(items.get(item_index).get(3));
            }
            viewHolder.tv_event_classroom.setText(items.get(item_index).get(4));
            if (resource_layout == R.layout.row_current || resource_layout == R.layout.row_day) {
                viewHolder.tv_event_lecturer.setText(items.get(item_index).get(5));
                switch (items.get(item_index).get(2)) {
                    case " CM":
                        viewHolder.img_event_type.setImageResource(R.drawable.ic_lecture_course);
                        break;
                    case " TD":
                        viewHolder.img_event_type.setImageResource(R.drawable.ic_tutorial_classes);
                        break;
                    case " TP":
                        viewHolder.img_event_type.setImageResource(R.drawable.ic_practicals);
                        break;
                    case " Evaluation":
                        viewHolder.img_event_type.setImageResource(R.drawable.ic_assessments);
                        break;
                    default: // Par défaut on ne met rien dans l'ImageView
                        if (items.get(item_index).get(3).contains("UEO"))
                            viewHolder.img_event_type.setImageResource(R.drawable.ic_sport);
                        else
                            viewHolder.img_event_type.setImageResource(android.R.color.transparent);
                        break;
                }
            }
        } else {
            if (resource_layout == R.layout.row_assessments)
                viewHolder.tv_assessment_date.setVisibility(View.GONE);
            if (resource_layout != R.layout.row_classrooms) {
                viewHolder.tv_event_start.setText("");
                viewHolder.tv_event_end.setText("");
                viewHolder.tv_event_course.setText(items.get(item_index).get(0));
            }
            viewHolder.tv_event_classroom.setText("");
            if (resource_layout == R.layout.row_current || resource_layout == R.layout.row_day) {
                viewHolder.img_event_type.setImageResource(android.R.color.transparent);
                viewHolder.tv_event_lecturer.setText("");
            }
        }
    }

    public void refresh_items(List<Matiere> matieres) {
        items.clear();
        if (!matieres.isEmpty()) {
            DateFormat df = new SimpleDateFormat("HH:mm");
            DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
            for (Matiere matiere : matieres)
                items.add(
                        new ArrayList<String>(asList(
                                df.format(matiere.getDebut()),
                                df.format(matiere.getFin()),
                                matiere.getType(),
                                matiere.getNom(),
                                matiere.getSalle(),
                                matiere.getProf(),
                                df2.format(matiere.getDebut())
                        ))
                );
        } else {
            if (resource_layout == R.layout.row_day)
                items.add(new ArrayList<String>(asList("Rien à afficher pour ce jour :)")));
            else
                items.add(new ArrayList<String>(asList("Rien à afficher aujourd'hui :)")));
        }
    }

    /**
     * Permet au système de savoir combien d'éléments sont stockés dans la présente liste
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return items.size();
    }
}
